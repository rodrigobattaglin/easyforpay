@extends('template')

@section('content')
	<h1 class="title-pg">Listagem de Operadores</h1>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!--a href="{{url('/painel/produtos/create')}}" class="btn btn-primary btn-add">
		<span class="glyphicon glyphicon-plus"></span>Cadastrar
	</a-->
	<a href="{{route('operador.create')}}" class="btn btn-primary btn-add">
		<span class="glyphicon glyphicon-plus"></span>Cadastrar
	</a>
	<table class="table table-striped">
		<tr>
			<th>Código</th>
			<th>Nome</th>
			<th>Email</th>
			<th>Ativo</th>
			<th width="100px">Ações</th>
		</tr>
		@foreach($operadores as $operador)
			<tr>
				<td>{{$operador->codigo}}</td>
				<td>{{$operador->nome}}</td>
				<td>{{$operador->email}}</td>
				<td>{{$operador->ativo}}</td>
				<td></td>
			</tr>
		@endforeach
	</table>
@endsection