@extends('template')

@section('content')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<h1 class="title-pg">
	<a href="{{route('operador.index')}}">
		<span class="glyphicon glyphicon-fast-backward"></span>
	</a>
	Gestão Operador: <b>Novo</b>
</h1>

@if(isset($errors) && count($errors) > 0)
	<div class="alert alert-danger">
		@foreach($errors as $error)
			<p>{{$error}}</p>
		@endforeach
	</div>
@endif

@if(isset($operador))
	{!! Form::model($operador, ['route' => ['operador.update', $operador->codigo], 'class' => 'form', 'method' => 'put']) !!}
@else
	{!! Form::open(['route' => 'operador.store', 'class' => 'form']) !!}
@endif
		{!! csrf_field() !!}
		<div class="form-group">
			{!! Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
		</div>
		<div class="form-group">
			{!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
		</div>
		<div class="form-group">
			{!! Form::text('senha', null, ['class' => 'form-control', 'placeholder' => 'Senha']) !!}
		</div>
		<div class="form-group">
			{!! Form::textarea('mensagem', null, ['class' => 'form-control', 'placeholder' => 'Mensagem']) !!}
		</div>
		{!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
	<!--/form-->
	{!! Form::close() !!}
@endsection