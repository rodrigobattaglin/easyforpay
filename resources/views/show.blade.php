@extends('templates.template')

@section('content')
	<h1 class="title-pg">
		<a href="{{route('operador.index')}}">
			<span class="glyphicon glyphicon-fast-backward"></span>
		</a>
		Nome: <b>{{$operador->nome}}</b>
	</h1>
	<p><b>Email: </b> {{$operador->email}}</p>
	<p><b>Senha: </b> {{$operador->senha}}</p>
	<p><b>Mensagem: </b> {{$operador->mensagem}}</p>
	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<hr>
	@if(isset($errors) && count($errors) > 0)
		<div class="alert alert-danger">
			@foreach($errors->all() as $error)
				<p>{{$error}}</p>
			@endforeach
		</div>
	@endif
	
@endsection