<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<title>{{$title or 'EFP'}}</title>

		<!--Bootstrap-->
		<link rel="stylesheet" href="{{url('assets/painel/css/style.css')}}">
	</head>
	<body>
		<div class="container">
			@yield('content')
		</div>
	</body>
</html>