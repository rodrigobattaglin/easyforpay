<?php

namespace EFP\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OperadorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'        => 'required | min:3 | max:64',
            'email'       => 'required | min:3 | max:64',
            'senha'       => 'required | min:3 | max:14',
            'mensagem'    => 'sometimes | nullable | min:3 | max:1000',
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Nome obrigatório',
            'nome.min' => 'Mínimo de 3 caracteres para nome',
            'nome.max' => 'Máximo de 100 caracteres para nome',
            'email.required' => 'Email obrigatório',
            'email.min' => 'Mínimo de 3 caracteres para email',
            'email.max' => 'Máximo de 100 caracteres para email',
            'senha.required' => 'Senha obrigatória',
            'senha.min' => 'Mínimo de 3 caracteres para senha',
            'senha.max' => 'Máximo de 100 caracteres para senha',
            'mensagem.min' => 'Mínimo de 3 caracteres para mensagem',
            'mensagem.max' => 'Máximo de 1000 caracteres para mensagem',
        ];
    }
}
