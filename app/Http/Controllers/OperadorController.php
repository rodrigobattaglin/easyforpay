<?php

namespace EFP\Http\Controllers;

use Illuminate\Http\Request;
use EFP\Operador;
use EFP\Http\Controllers\Api as API;
use EFP\Http\Requests\OperadorFormRequest;

class OperadorController extends Controller
{
    private $operadores;
    private $api;

    public function __construct(Operador $operador)
    {
        $this->operadores = $operador;
        $this->api = new API();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'EasyforPay - Operadores';
        try
        {
            $response = json_decode($this->api->call('operador', 'GET'));
            $operadores = $response->objeto;
            //dd($operadores);
            return view('operador.index', compact('operadores', 'title'));
        }
        catch(Exception $e)
        {
            return view('operador.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "EasyforPay - Novo Operador";
        return view('operador.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OperadorFormRequest $request)
    {
        $errors = [];
        try
        {
            $dataForm = $request->all();
            unset($dataForm['_token']);
            $dataForm = json_encode($dataForm);
            $response = json_decode($this->api->call('operador', 'POST', $dataForm, false));
            //dd($response);
            if($response->successo == "1") return redirect()->route('operador.index');
            else
            {
                array_push($errors, $response->erro->codigo." ".$response->erro->mensagem);
                return redirect()->back()->with('errors', $errors);
            }
        }
        catch(Exception $e){
            array_push($errors, $e);
            return redirect()->back()->with('errors', $errors);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
