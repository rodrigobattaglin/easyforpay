<?php

namespace EFP;

use Illuminate\Database\Eloquent\Model;

class Operador extends Model
{
    protected $fillable = ['nome', 'email', 'senha', 'mensagem'];
}
